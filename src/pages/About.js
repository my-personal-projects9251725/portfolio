import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';


const AboutMe = () => {
  return (
    <div className="container aboutme-container">
      <div className="row">
        <div className="col-md-4">
          <div className="card">
                <div className="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" height="38px" width="38px" version="1.1" id="heart" viewBox="0 0 471.701 471.701" xmlSpace="preserve">
                    <linearGradient id="gradientColor">
                      <stop offset="5%" stopColor="#7eaaff"></stop>
                      <stop offset="95%" stopColor="#ff48fb"></stop>
                    </linearGradient>
                    <g>
                      <path d="M433.601,67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7,13.6-92.4,38.3l-12.9,12.9l-13.1-13.1   c-24.7-24.7-57.6-38.4-92.5-38.4c-34.8,0-67.6,13.6-92.2,38.2c-24.7,24.7-38.3,57.5-38.2,92.4c0,34.9,13.7,67.6,38.4,92.3   l187.8,187.8c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-3.9l188.2-187.5c24.7-24.7,38.3-57.5,38.3-92.4   C471.801,124.501,458.301,91.701,433.601,67.001z M414.401,232.701l-178.7,178l-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3   s10.7-53.7,30.3-73.2c19.5-19.5,45.5-30.3,73.1-30.3c27.7,0,53.8,10.8,73.4,30.4l22.6,22.6c5.3,5.3,13.8,5.3,19.1,0l22.4-22.4   c19.6-19.6,45.7-30.4,73.3-30.4c27.6,0,53.6,10.8,73.2,30.3c19.6,19.6,30.3,45.6,30.3,73.3   C444.801,187.101,434.001,213.101,414.401,232.701z"></path>
                    </g>
                  </svg>
                </div>
                <p className="title">Who am I?</p>
                <p className="text">Hi! I'm Roque Jerico Turingan a seasoned Frontend Software Engineer, currently engaged in a dynamic role at Manulife.</p>
              </div>
        </div>

        <div className="col-md-4">
          <div className="card">
                <div className="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" height="50px" width="38px" version="1.1" id="heart" viewBox="0 0 471.701 471.701" xmlSpace="preserve">
                    <linearGradient id="gradientColor">
                      <stop offset="5%" stopColor="#7eaaff"></stop>
                      <stop offset="95%" stopColor="#ff48fb"></stop>
                    </linearGradient>
                    <g>
                      <path d="M433.601,67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7,13.6-92.4,38.3l-12.9,12.9l-13.1-13.1   c-24.7-24.7-57.6-38.4-92.5-38.4c-34.8,0-67.6,13.6-92.2,38.2c-24.7,24.7-38.3,57.5-38.2,92.4c0,34.9,13.7,67.6,38.4,92.3   l187.8,187.8c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-3.9l188.2-187.5c24.7-24.7,38.3-57.5,38.3-92.4   C471.801,124.501,458.301,91.701,433.601,67.001z M414.401,232.701l-178.7,178l-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3   s10.7-53.7,30.3-73.2c19.5-19.5,45.5-30.3,73.1-30.3c27.7,0,53.8,10.8,73.4,30.4l22.6,22.6c5.3,5.3,13.8,5.3,19.1,0l22.4-22.4   c19.6-19.6,45.7-30.4,73.3-30.4c27.6,0,53.6,10.8,73.2,30.3c19.6,19.6,30.3,45.6,30.3,73.3   C444.801,187.101,434.001,213.101,414.401,232.701z"></path>
                    </g>
                  </svg>
                </div>
                <p className="title">Education</p>
                <p className="text">Successfully earned a Bachelor's Degree in Computer Engineering at National College of Science and Technology.</p>
              </div>
        </div>

        <div className="col-md-4">
          <div className="card">
                <div className="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" height="38px" width="38px" version="1.1" id="heart" viewBox="0 0 471.701 471.701" xmlSpace="preserve">
                    <linearGradient id="gradientColor">
                      <stop offset="5%" stopColor="#7eaaff"></stop>
                      <stop offset="95%" stopColor="#ff48fb"></stop>
                    </linearGradient>
                    <g>
                      <path d="M433.601,67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7,13.6-92.4,38.3l-12.9,12.9l-13.1-13.1   c-24.7-24.7-57.6-38.4-92.5-38.4c-34.8,0-67.6,13.6-92.2,38.2c-24.7,24.7-38.3,57.5-38.2,92.4c0,34.9,13.7,67.6,38.4,92.3   l187.8,187.8c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-3.9l188.2-187.5c24.7-24.7,38.3-57.5,38.3-92.4   C471.801,124.501,458.301,91.701,433.601,67.001z M414.401,232.701l-178.7,178l-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3   s10.7-53.7,30.3-73.2c19.5-19.5,45.5-30.3,73.1-30.3c27.7,0,53.8,10.8,73.4,30.4l22.6,22.6c5.3,5.3,13.8,5.3,19.1,0l22.4-22.4   c19.6-19.6,45.7-30.4,73.3-30.4c27.6,0,53.6,10.8,73.2,30.3c19.6,19.6,30.3,45.6,30.3,73.3   C444.801,187.101,434.001,213.101,414.401,232.701z"></path>
                    </g>
                  </svg>
                </div>
                <p className="title">Hobbies</p>
                <p className="text">Beyond coding, I find inspiration and relaxation in hiking, exploring natures, reading, and playing the guitar.</p>
              </div>
        </div>
      </div>
      
      <h2 className="mt-5 text-center">Timeline of Experience</h2>
      <ul className="timeline">

      <li>
          <div className="direction-l">
            <div className="flag-wrapper">
              <span className="flag">Manulife</span>
              <span className="time-wrapper">
                <span className="time">November 2023 - present</span>
              </span>
            </div>
            <div className="desc">
              Currently employed as Front-end Software Engineer at Manulife.
            </div>
          </div>
        </li>

        <li>
          <div className="direction-r">
            <div className="flag-wrapper">
              <span className="flag">Zuitt Coding Bootcamp & NCST</span>
              <span className="time-wrapper">
                <span className="time">2023</span>
              </span>
            </div>
            <div className="desc">
            My journey into the world of software development began with a solid educational foundation, having earned a Bachelor's Degree in Computer Engineering at National College of Science and Technology in September 2023. To further enhance my skills and stay at the forefront of the industry, I undertook a rigorous 3-month intensive coding bootcamp at Zuitt where I earned the Best in Capstone 2 award and best in prototype in thesis.
            </div>
          </div>
        </li>

        <li>
          <div className="direction-l">
            <div className="flag-wrapper">
              <span className="flag">Your Virtual World</span>
              <span className="time-wrapper">
                <span className="time">2021-2023</span>
              </span>
            </div>
            <div className="desc">
              I gained valuable professional experience at Your Virtual World (YVW), where I worked as a Customer Service Representative and was later promoted to the role of Subject Matter Expert. Furthermore, I served as an IT Support Specialist intern at YVW for five months, acquiring hands-on experience in troubleshooting and supporting various technical systems.
            </div>
          </div>
        </li>

        <li>
          <div className="direction-r">
            <div className="flag-wrapper">
              <span className="flag">NCST SHS</span>
              <span className="time-wrapper">
                <span className="time">2016-2018</span>
              </span>
            </div>
            <div className="desc">
              I completed my Senior High School education at the National College of Science and Technology, specializing in the Humanities and Social Sciences Strand. I was honored with the distinction of being a With High Honors recipient and recognized as a Leadership Awardee. Additionally, I held the positions of Editor-in-Chief in our school publication and President of SHS Student Government.
            </div>
          </div>
        </li>

        <li>
          <div className="direction-l">
            <div className="flag-wrapper">
              <span className="flag">Campus Journalism Trainer</span>
              <span className="time-wrapper">
                <span className="time">2014-2023</span>
              </span>
            </div>
            <div className="desc">
              I gained valuable part-time experience as a Campus Journalism Trainer, where I worked with students to develop their writing and communication skills. From 2014 to 2023, I conducted workshops, mentored aspiring journalists, and organized various campus journalism events.
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default AboutMe;
